'use strict';

/**
 * @ngdoc overview
 * @name AirBnBrewApp
 * @description
 * # AirBnBrewApp
 *
 * Main module of the application.
 */
angular
  .module('AirBnBrewApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider, $locationProvider) {
    $locationProvider.hashPrefix('');
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'vm'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'vm'
      })
      .when('/reservation', {
        templateUrl: 'views/reservation.html',
        controller: 'ReservationCtrl',
        controllerAs: 'vm'
      })
      .when('/brewery', {
        templateUrl: 'views/brewery.html',
        controller: 'BreweryCtrl',
        controllerAs: 'vm'
      })
      .when('/brewery-list', {
        templateUrl: 'views/brewery-list.html',
        controller: 'BreweryListCtrl',
        controllerAs: 'vm'
      })
      .when('/brewer', {
        templateUrl: 'views/brewer.html',
        controller: 'BrewerCtrl',
        controllerAs: 'vm'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
