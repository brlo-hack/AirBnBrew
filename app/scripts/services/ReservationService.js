(function () {
  'use strict';

  angular
    .module('AirBnBrewApp')
    .factory('ReservationService', ReservationService);

  ReservationService.$inject = ['$resource'];

  function ReservationService($resource) {
    return $resource('http://csvikings-node-red.iot.mycsn.be/airBnBrew/reservation', {
      reservationId: "@id"
    });
  }
})();
