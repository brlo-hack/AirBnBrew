(function () {
  'use strict';

  angular
    .module('AirBnBrewApp')
    .factory('BrewerService', BrewerService);

  BrewerService.$inject = ['$resource'];

  function BrewerService($resource) {
    return $resource('http://csvikings-node-red.iot.mycsn.be/airBnBrew/brewer', {
      brewerId: "@id"
    });
  }
})();
