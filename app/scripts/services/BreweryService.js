(function () {
  'use strict';

  angular
    .module('AirBnBrewApp')
    .factory('BreweryService', BreweryService);

  BreweryService.$inject = ['$resource'];

  function BreweryService($resource ) {
    var restPath = 'http://csvikings-node-red.iot.mycsn.be/airBnBrew';
    return $resource(restPath+'/brewery/list', {},{
      getList: {
        method: 'GET',
        url: restPath+'/brewery/list',
        isArray: true,
        headers: {'Authorization': 'Basic Y3N2aWtpbmdzOjBfdGltZV9XUklUVEVOX21hcmtfNA=='}
      },
      saveBrewery: {
        method: 'POST',
        url: restPath+'/brewery',
        headers: {'Authorization': 'Basic Y3N2aWtpbmdzOjBfdGltZV9XUklUVEVOX21hcmtfNA=='}
      },
      getReservationType: {
        method: 'GET',
        url: restPath+'/brewery/list',
        isArray: true,
        headers: {'Authorization': 'Basic Y3N2aWtpbmdzOjBfdGltZV9XUklUVEVOX21hcmtfNA=='}
      },
      getBrewType: {
        method: 'GET',
        url: restPath+'/brewery/list',
        isArray: true,
        headers: {'Authorization': 'Basic Y3N2aWtpbmdzOjBfdGltZV9XUklUVEVOX21hcmtfNA=='}
      }
    });
  }
})();
