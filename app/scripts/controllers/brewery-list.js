(function () {
  'use strict';

  angular
    .module('AirBnBrewApp')
    .controller('BreweryListCtrl', BreweryListCtrl);

  BreweryListCtrl.$inject = ['BreweryService', '$http'];

  function BreweryListCtrl(BreweryService, $http) {
    var vm = this;
    vm.promise = {};
    vm.breweryListModel = [];
    init();

    function init() {
      getBreweryList();
    }

    function getBreweryList() {
      vm.promise = BreweryService.getList().$promise.then(function (data) {
        vm.breweryListModel = data
      });

    }

  }

})();
