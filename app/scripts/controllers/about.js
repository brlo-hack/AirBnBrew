'use strict';

/**
 * @ngdoc function
 * @name AirBnBrewApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the AirBnBrewApp
 */
angular.module('AirBnBrewApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
