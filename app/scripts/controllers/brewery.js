(function () {
  'use strict';

  angular
    .module('AirBnBrewApp')
    .controller('BreweryCtrl', BreweryCtrl);

  BreweryCtrl.$inject = ['BreweryService'];

  function BreweryCtrl(BreweryService) {
    var vm = this;
    vm.promise = {};
    vm.breweryModel = {};
    vm.tmpLogo = null;
    vm.saveBrewery = saveBrewery;
    vm.showSuccessAlert = false;
    vm.showErrorAlert = false;
    vm.switchSuccess = switchSuccess;
    vm.switchError = switchError;
    init();

    function init() {

    }

    function switchSuccess () {
      vm.showSuccessAlert = !vm.showSuccessAlert;
    }

    function switchError() {
      vm.showErrorAlert = !vm.showErrorAlert;
    }

    function saveBrewery() {
      BreweryService.saveBrewery(vm.breweryModel).$promise
        .then(function () {
          vm.breweryModel = {};
          vm.showSuccessAlert = true;
        },function(error){
          vm.showErrorAlert = true;
        });

    }

  }

})();
