'use strict';

/**
 * @ngdoc function
 * @name AirBnBrewApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the AirBnBrewApp
 */
angular.module('AirBnBrewApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
